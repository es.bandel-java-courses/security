package org.example.framework.server.http;

import lombok.Builder;
import lombok.Singular;
import lombok.extern.slf4j.Slf4j;
import org.example.framework.security.auth.SecurityContext;
import org.example.framework.server.controller.handler.ReturnValueHandler;
import org.example.framework.server.controller.resolver.ArgumentResolver;
import org.example.framework.server.exception.*;
import org.example.framework.server.handler.Handler;
import org.example.framework.server.middleware.Middleware;
import org.example.framework.server.router.MethodRoute;
import org.example.framework.server.router.MethodRouteAdapter;
import org.example.framework.server.router.MethodRouter;
import org.example.framework.server.util.Bytes;

import javax.net.ServerSocketFactory;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

@Slf4j
@Builder
public class Server {
    private static final int MAX_REQUEST_LINE_AND_HEADERS_SIZE = 4096;
    private static final byte[] CRLF = new byte[]{'\r', '\n'};
    private static final byte[] CRLFCRLF = new byte[]{'\r', '\n', '\r', '\n'};
    private static final int MAX_CONTENT_LENGTH = 10 * 1024 * 1024;

    private final AtomicInteger workerCounter = new AtomicInteger();
    private final ExecutorService workers = Executors.newFixedThreadPool(64, r -> {
        final Thread worker = new Thread(r);
        worker.setName("worker-" + workerCounter.incrementAndGet());
        return worker;
    });

    @Singular
    private final List<Middleware> middlewares;
    @Singular
    private final List<ArgumentResolver> argumentResolvers;
    @Singular
    private final List<ReturnValueHandler> returnValueHandlers;

    private final MethodRouter router;
    @Singular
    // String - path, String - method
    private final Map<Pattern, Map<HttpMethods, Handler>> routes;
    @Builder.Default
    private final Handler notFoundHandler = Handler::notFoundHandler;
    @Builder.Default
    private final Handler methodNotAllowed = Handler::methodNotAllowedHandler;
    @Builder.Default
    private final Handler internalServerErrorHandler = Handler::internalServerError;

    private ServerSocket serverSocket;

    public void start(final int port) {
        Thread thread = new Thread(() -> {
            try {
                serveHTTPS(port);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        thread.start();
    }

    public void stop() throws IOException {
        try {
            workers.shutdown();
            final boolean done = workers.awaitTermination(5, TimeUnit.SECONDS);
            if (!done) {
                workers.shutdownNow();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            serverSocket.close();
        }
    }

    public void serveHTTPS(final int port) throws IOException {
        final ServerSocketFactory socketFactory = ServerSocketFactory.getDefault();
        serverSocket = socketFactory.createServerSocket(port);

        log.info("server listen on {}", port);
        while (true) {
            try {
                final Socket socket = serverSocket.accept();
                workers.submit(() -> handle(socket));
            } catch (SocketException e) {
                log.error("socket was closed");
                break;
            } catch (Exception e) {
                log.error("some error", e);
            }
        }
    }

    private void handle(final Socket acceptedSocket) {
        try (
                final Socket socket = acceptedSocket;
                final InputStream in = new BufferedInputStream(socket.getInputStream());
                final OutputStream out = socket.getOutputStream()
        ) {
            log.debug("client connected: {}:{}", socket.getInetAddress(), socket.getPort());
            final Request request = new Request();
            final Response response = new Response(out);
            try {
                final byte[] buffer = new byte[MAX_REQUEST_LINE_AND_HEADERS_SIZE];
                if (!in.markSupported()) {
                    throw new MarkNotSupportedException();
                }
                in.mark(MAX_REQUEST_LINE_AND_HEADERS_SIZE);

                final int read = in.read(buffer);
                final int requestLineEndIndex = Bytes.indexOf(buffer, CRLF);
                if (requestLineEndIndex == -1) {
                    throw new InvalidRequestStructureException("request line end index not found");
                }
                log.debug("request line end index: {}", requestLineEndIndex);
                final String requestLine = new String(buffer, 0, requestLineEndIndex, StandardCharsets.UTF_8);
                log.debug("request line: {}", requestLine);
                final String[] requestLineParts = requestLine.split("\\s+", 3);
                if (requestLineParts.length != 3) {
                    throw new InvalidRequestLineStructureException(requestLine);
                }

                request.setMethod(requestLineParts[0]);
                request.setHttpVersion(requestLineParts[2]);

                final String pathAndQuery = URLDecoder.decode(requestLineParts[1], StandardCharsets.UTF_8.name());

                final String[] pathAndQueryParts = pathAndQuery.split("\\?", 2);

                final String requestPath = pathAndQueryParts[0];
                request.setPath(requestPath);
                if (pathAndQueryParts.length == 2) {
                    request.setQuery(pathAndQueryParts[1]);
                }

                final int headersStartIndex = requestLineEndIndex + CRLF.length;
                final int headersEndIndex = Bytes.indexOf(buffer, CRLFCRLF, headersStartIndex);
                if (headersEndIndex == -1) {
                    throw new InvalidRequestStructureException("header end index not found");
                }

                int lastProcessedIndex = headersStartIndex;
                int contentLength = 0;

                while (lastProcessedIndex < headersEndIndex - CRLF.length) {
                    final int currentHeaderEndIndex = Bytes.indexOf(buffer, CRLF, lastProcessedIndex);
                    final String currentHeaderLine = new String(buffer, lastProcessedIndex, currentHeaderEndIndex - lastProcessedIndex);
                    lastProcessedIndex = currentHeaderEndIndex + CRLF.length;

                    final String[] headerParts = currentHeaderLine.split(":\\s*", 2);
                    if (headerParts.length != 2) {
                        throw new InvalidHeaderLineStructureException(currentHeaderLine);
                    }
                    request.getHeaders().put(headerParts[0], headerParts[1]);

                    if (!headerParts[0].equalsIgnoreCase(HttpHeaders.CONTENT_LENGTH.value())) {
                        continue;
                    }

                    contentLength = Integer.parseInt(headerParts[1]);
                    log.debug("content-length: {}", contentLength);
                }

                if (contentLength > MAX_CONTENT_LENGTH) {
                    throw new RequestBodyTooLargeException();
                }

                final int bodyStartIndex = headersEndIndex + CRLFCRLF.length;
                in.reset();
                final long skipped = in.skip(bodyStartIndex);

                final byte[] body = new byte[contentLength];
                final int bodyRead = in.read(body);

                request.setBody(body);

                in.reset();

                for (final Middleware middleware : middlewares) {
                    middleware.handle(socket, request);
                }

                final MethodRoute route = router.findController(request.getPath(), request.getMethod())
                        .orElseThrow(ResourceNotFoundException::new);

                request.setPathMatcher(route.getMatcher());

                final MethodRouteAdapter adapter = new MethodRouteAdapter(route, argumentResolvers, returnValueHandlers);
                adapter.handle(request, response);
            } catch (MethodNotAllowedException e) {
                log.error("request method not allowed", e);
                methodNotAllowed.handle(request, response);
            } catch (ResourceNotFoundException e) {
                log.error("can't found request", e);
                notFoundHandler.handle(request, response);
            } catch (Exception e) {
                log.error("can't handle request", e);
                internalServerErrorHandler.handle(request, response);
            }
        } catch (Exception e) {
            log.error("can't handle request", e);
        } finally {
            SecurityContext.clear();
        }
    }
}
