package org.example.framework.security.auth;

import lombok.Data;

import java.security.Principal;

@Data
public class SecurityContext {
    private static final ThreadLocal<Principal> principalHolder = new ThreadLocal<>();
    private static final ThreadLocal<Auth> securityContext = new ThreadLocal<>();

    private SecurityContext() {
    }

    public static Auth getSecurityContext() {
        return securityContext.get();
    }

    public static void setSecurityContext(final Auth auth) {

        securityContext.set(auth);
    }

    public static Principal getPrincipal() {
        return principalHolder.get();
    }

    public static void setPrincipal(final Principal principal) {

        principalHolder.set(principal);
    }

    public static void clear() {
        principalHolder.remove();
    }
}

