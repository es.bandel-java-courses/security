package org.example.framework.security.auth;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Auth {
    private final String name;
    private final String role;
}
