package org.example.framework.security.postprocessor;

import lombok.extern.slf4j.Slf4j;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import org.example.framework.di.processor.BeanPostProcessor;
import org.example.framework.security.annotation.Audit;

import java.util.Arrays;
import java.util.List;

@Slf4j
public class AuditBeanPostProcessor implements BeanPostProcessor {
    @Override
    public Object process(Object object, Class<?> originalClazz, List<Object> arguments, List<Class<?>> argumentTypes) {
        if (Arrays.stream(originalClazz.getDeclaredMethods()).noneMatch(o -> o.isAnnotationPresent(Audit.class))) {
            return object;
        }

        final Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(originalClazz);
        enhancer.setCallback((MethodInterceptor) (obj, method, args, proxy) -> {
            if (!method.isAnnotationPresent(Audit.class)) {
                return method.invoke(object, args);
            }

            log.debug("method invoked: {}", method.getName());
            return method.invoke(object, args);
        });

        return enhancer.create(argumentTypes.toArray(new Class[0]), arguments.toArray());
    }
}
