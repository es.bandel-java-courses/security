package org.example.framework.security.postprocessor;

import lombok.extern.slf4j.Slf4j;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import org.example.framework.di.processor.BeanPostProcessor;
import org.example.framework.security.annotation.Audit;
import org.example.framework.security.annotation.HasRole;
import org.example.framework.security.auth.SecurityContext;
import org.example.framework.security.exception.AccessDeniedException;

import java.security.Principal;
import java.util.Arrays;
import java.util.List;

@Slf4j
public class HasRoleBeanPostProcessor implements BeanPostProcessor {
    @Override
    public Object process(Object object, Class<?> originalClazz, List<Object> arguments, List<Class<?>> argumentTypes) {
        if (Arrays.stream(originalClazz.getDeclaredMethods()).noneMatch(o -> o.isAnnotationPresent(HasRole.class))) {
            return object;
        }

        final Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(originalClazz);
        enhancer.setCallback((MethodInterceptor) (obj, method, args, proxy) -> {
            if (!method.isAnnotationPresent(HasRole.class)) {
                return method.invoke(object, args);
            }

            log.debug("method secured: {}", method.getName());
            final HasRole annotation = method.getAnnotation(HasRole.class);
            final String role = annotation.value();

            final Principal principal = SecurityContext.getPrincipal();
            if (!principal.getName().equals(role)) {
                log.warn("access denied for {} to method {}", principal.getName(), method.getName());
                throw new AccessDeniedException();
            }

            log.debug("access allowed for {} to method {}", principal.getName(), method.getName());
            return method.invoke(object, args);
        });

        return enhancer.create(argumentTypes.toArray(new Class[0]), arguments.toArray());
    }
}
