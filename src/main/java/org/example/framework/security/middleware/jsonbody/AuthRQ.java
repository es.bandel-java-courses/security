package org.example.framework.security.middleware.jsonbody;

import lombok.Data;

@Data
public class AuthRQ {
    private String username;
    private String password;
}
